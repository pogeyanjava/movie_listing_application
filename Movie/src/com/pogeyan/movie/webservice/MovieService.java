package com.pogeyan.movie.webservice;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.pogeyan.movie.pojo.Movie;

@Path("/movie")
public class MovieService {

	private static Map<String, Movie> movies = new HashMap<String, Movie>();  
    
    static {  
          
        Movie movie1 = new Movie();  
        movie1.setMovieId("11");  
        movie1.setMovieName("Movie App1");  
        movie1.setDescription("Description will come later..");
        movie1.setImageurl("/usr/home/sample/movie1.jpg");
        movie1.setRating(5);
        
        movies.put(movie1.getMovieId(), movie1);  
          
        Movie movie2 = new Movie();  
        movie2.setMovieId("22");  
        movie2.setMovieName("Movie App2");  
        movie2.setDescription("Description will see later..");
        movie2.setImageurl("/usr/home/sample/movie1.jpg");
        movie2.setRating(4);
        
        movies.put(movie2.getMovieId(), movie2);  
          
    }  
	
	@GET
    @Produces("text/plain")  
    public String hello(){  
        return "This is a sample test webservice...";      
    }  
    
	@GET  
    @Path("/mv")  
    @Produces("application/json")
	public Movie getMovieDetailsById(){
		return movies.get("11");
	}
	 /*@GET  
     @Path("{movieId}")  
     @Produces("application/json")
	public String getMovieDetailsById(@PathParam("movieId")String movieId){
		return movies.get(movieId).getDescription();
	}*/
	
}
