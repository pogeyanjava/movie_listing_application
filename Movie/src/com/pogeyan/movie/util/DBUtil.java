package com.pogeyan.movie.util;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.pogeyan.movie.pojo.User;

public class DBUtil {
         
	private static Connection connection = null;
	
	public static Connection getConnection(String driverClass, String url, String userName, String password){
		try
		{
			if(connection == null){
				ComboPooledDataSource dataSource = new ComboPooledDataSource();
				dataSource.setDriverClass(driverClass);
				dataSource.setJdbcUrl(url);
				dataSource.setUser(userName);
				dataSource.setPassword(password);
				dataSource.setInitialPoolSize(3);
				dataSource.setMaxPoolSize(10);
				connection = dataSource.getConnection();
				System.out.println("Got Connection returning back....");
			}	
		}catch(Exception e){
			e.printStackTrace();
		}		
		return connection;
	}
	
	public static Connection getConnection(){
		Properties properties = loadProperties();
		if(properties != null)
			return getConnection(properties.getProperty(Constant.DRIVER_CLASS_PROP),properties.getProperty(Constant.URL_PROP),
					             properties.getProperty(Constant.USER_PROP), properties.getProperty(Constant.PASSWORD_RPOP));
		return null;
	}
	
	private static Properties loadProperties(){
		Properties properties = new Properties();
		try{
			InputStream stream = DBUtil.class.getResourceAsStream("connectionResources.properties");
			if(stream != null){
				properties.load(stream);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return properties;
	}
	
	public static String prepareSelectQueryByMailId(String mailId){
		String selectQuery = "SELECT USERID, FIRST_NAME, LAST_NAME, PASSWORD, EMAILID, LAST_LOGIN_DATE FROM USER WHERE "
				             + "EMAILID = '"+mailId+"'";
		return selectQuery;
	}
	
	public static String prepareUpdateQueryForUser(User user){
		String updateQuery = "UPDATE USER SET LAST_LOGIN_DATE = " +user.getLastLoginDate()+ " WHERE EMAILID = '" +user.getEmailId()+ "'"
				+ " AND USERID = '" +user.getUserId()+"'";
		return updateQuery;
	}

	public static String prepareInsertQueryForUser(User user){
		String insertQuery = "INSERT INTO USER(USERID, FIRST_NAME, LAST_NAME, PASSWORD, EMAILID, CREATION_DATE) "
				             + "VALUES('" +user.getUserId()+ "','" +user.getFirstName()+ "','" +user.getLastName()+ "','" +user.getPassword()+ "','"
				             + "" +user.getEmailId()+ "'," +user.getCreatedDate()+ ")";
		return insertQuery;
	}
}
