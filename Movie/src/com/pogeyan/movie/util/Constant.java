package com.pogeyan.movie.util;

public class Constant {

	public final static String DRIVER_CLASS_PROP = "driverClassName";
	public final static String URL_PROP = "url";
	public final static String USER_PROP = "userName";
	public final static String PASSWORD_RPOP = "password";
	public final static String EMAIL_EXISTS_ERROR = "Entered mailid already exists, Please enter valid mailid..";
	public final static String REGISTER_SUCC_MSG = "You've been registered successfully..";
	public final static String LOGIN_SUCCESS_MSG = "You're loggedin successfully...";
	public final static String LOGIN_ERROR_MSG = "Please check entered mailid/password..";
}
