package com.pogeyan.movie.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UniqueKeyGenerator {

	public static String getUniqueKey(String className){
		if(className.equals("com.pogeyan.movie.pojo.User"))
			return generateUID("USR");
		else if(className.equals("com.pogeyan.movie.pojo.User"))
			return generateUID("MOV");
		else
			return "";
	}
	
	private static String generateUID(String classKey){

		StringBuffer uniqueString = new StringBuffer();
		Random r = new Random();
		List<Integer> codes = new ArrayList<>();
		for (int i = 0; i < 10; i++)
		{
		    int x = r.nextInt(9999);
		    while (codes.contains(x))
		        x = r.nextInt(9999);
		    codes.add(x);
		}
		uniqueString.append(classKey);
		uniqueString.append(String.format("%04d", codes.get(0)));
		return uniqueString.toString();
	}
}
