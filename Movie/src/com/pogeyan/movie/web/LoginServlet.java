package com.pogeyan.movie.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import com.pogeyan.movie.dao.impl.UserDAOImpl;
import com.pogeyan.movie.pojo.User;
import com.pogeyan.movie.util.Constant;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public LoginServlet() {
        super();        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String emailId = request.getParameter("emailID");
		String password = request.getParameter("password");
		JSONArray array = new JSONArray();
		JSONObject object = new JSONObject();
		try{
			if(emailId != null && emailId.length() != 0 && password != null && password.length() != 0){
				User user = UserDAOImpl.getInstance().getUserByMailId(emailId);
				if(user != null && user.getPassword().equals(password)){
					Map<String, String> successMsg = new HashMap<String, String>();
					session.setAttribute("emailId", emailId);
					user.setLastLoginDate(new Date().getTime());
					UserDAOImpl.getInstance().updateUser(user);
					successMsg.put("SUCCESS_CODE", "0");
					successMsg.put("SUCCESS_MSG", Constant.LOGIN_SUCCESS_MSG);
					object.put("LOGIN_SUCCESS", successMsg);
					array.put(object);
					//response.sendRedirect("movies.jsp");
				}else{
					Map<String, String> errorMsg = new HashMap<String, String>();
					errorMsg.put("ERROR_CODE", "1");
					errorMsg.put("ERROR_MSG", Constant.LOGIN_ERROR_MSG);
					object.put("LOGIN_ERROR", errorMsg);
					array.put(object);
					//response.sendRedirect("login.jsp");
				}
				response.getWriter().print(array);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
