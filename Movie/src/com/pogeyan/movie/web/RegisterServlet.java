package com.pogeyan.movie.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import com.pogeyan.movie.dao.impl.UserDAOImpl;
import com.pogeyan.movie.pojo.User;
import com.pogeyan.movie.util.Constant;
import com.pogeyan.movie.util.EncryptionDecryptionUtil;
import com.pogeyan.movie.util.UniqueKeyGenerator;

@WebServlet(name = "register", urlPatterns = { "/register" })
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RegisterServlet() {
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String password = request.getParameter("password");
		String mailId = request.getParameter("emailId");	
		JSONArray array = new JSONArray();
		JSONObject object = new JSONObject();
		try{
			if(mailId != null && mailId.length() != 0){			
				User masterUser = UserDAOImpl.getInstance().getUserByMailId(mailId);
				if(masterUser != null && masterUser.getEmailId() != null && mailId.equals(masterUser.getEmailId())){	
					Map<String,String> errorMsg = new HashMap<String,String>();
					errorMsg.put("ERROR_CODE", "1");
					errorMsg.put("ERROR_MSG", Constant.EMAIL_EXISTS_ERROR);
					object.put("MAILID_EXISTS_ERROR", errorMsg);
					array.put(object);
					//response.sendRedirect("registration.jsp");
				}else{
					if(password != null && password.length() != 0 && 
							mailId != null && mailId.length() != 0){
						User user = new User();
						user.setUserId(UniqueKeyGenerator.getUniqueKey(User.class.getName()));
						user.setEmailId(mailId);
						user.setPassword(EncryptionDecryptionUtil.getInstance().encrypt(password));
						user.setFirstName(firstName);
						user.setLastName(lastName);
						user.setCreatedDate(new Date().getTime());
						UserDAOImpl.getInstance().saveUser(user);
						Map<String,String> errorMsg = new HashMap<String,String>();
						errorMsg.put("SUCCESS_CODE", "0");
						errorMsg.put("SUCCESS_MSG", Constant.REGISTER_SUCC_MSG);
						object.put("REGISTER_SUCCESS", errorMsg);
						array.put(object);
						//response.sendRedirect("login.jsp");						
					}
				}
					
			}else{
				response.getWriter().println("I got empty field, pls send valid data...");
			}	
			response.getWriter().print(array);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
