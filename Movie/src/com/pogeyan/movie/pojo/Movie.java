package com.pogeyan.movie.pojo;

public class Movie {

	private String movieId;
	private String movieName;
	private String description;
	private String imageurl;
	private Integer rating;
	
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	@Override
	public String toString(){
		return new StringBuffer("MovieID : ").append(this.movieId).append("MovieName : ").append(this.movieName).
				append("Description : ").append(this.description).append("ImageUrl : ").append(this.imageurl).
				append("Rating : ").append(this.rating).toString();
	}
}
