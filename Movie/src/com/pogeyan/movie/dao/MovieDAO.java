package com.pogeyan.movie.dao;

import java.util.List;

import com.pogeyan.movie.pojo.Movie;

public interface MovieDAO {
	List<Movie> getMovies();
	Movie getMovieDetailsById(String id);
}
