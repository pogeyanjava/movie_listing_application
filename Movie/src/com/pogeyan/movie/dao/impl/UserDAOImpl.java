package com.pogeyan.movie.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;

import com.pogeyan.movie.dao.UserDAO;
import com.pogeyan.movie.pojo.User;
import com.pogeyan.movie.util.DBUtil;
import com.pogeyan.movie.util.EncryptionDecryptionUtil;

public class UserDAOImpl implements UserDAO{

	private static UserDAO userDAO = null;
	
	@Override
	public void saveUser(User user) {
		try{
			Connection connection = DBUtil.getConnection();
			connection.createStatement().executeUpdate(DBUtil.prepareInsertQueryForUser(user));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void updateUser(User user) {
		try{
			Connection connection = DBUtil.getConnection();
			connection.createStatement().executeUpdate(DBUtil.prepareUpdateQueryForUser(user));
		}catch(Exception e){
			e.printStackTrace();
		}	
	}

	@Override
	public User getUserByMailId(String emailId) {
		User user = null;
		try{
			Connection connection = DBUtil.getConnection();			
			ResultSet set = connection.createStatement().executeQuery(DBUtil.prepareSelectQueryByMailId(emailId));
			if(set != null){
				user = new User();
				while(set.next()){
					user.setUserId(set.getString("USERID"));
					user.setEmailId(set.getString("EMAILID"));
					user.setLastLoginDate(set.getLong("LAST_LOGIN_DATE"));
					user.setFirstName(set.getString("FIRST_NAME"));
					user.setLastName(set.getString("LAST_NAME"));
					user.setPassword(EncryptionDecryptionUtil.getInstance().decrypt(set.getString("PASSWORD")));
				}								
			}
		}catch(Exception e){
			e.printStackTrace();
		}
        return user;
	}
	
	public static UserDAO getInstance(){
		if(userDAO == null)
			userDAO = new UserDAOImpl();
		return userDAO;
	}

}
