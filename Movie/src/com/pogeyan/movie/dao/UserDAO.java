package com.pogeyan.movie.dao;

import com.pogeyan.movie.pojo.User;

public interface UserDAO {

	void saveUser(User user);
	void updateUser(User user);
	User getUserByMailId(String emailId);
}
