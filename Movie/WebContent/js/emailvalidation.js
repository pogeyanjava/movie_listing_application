$(document).ready(function(e) 
{
    $('#submit').click(function() 
    		{
	    		function validateEmail(InputEmailAddress,ConfirmEmailAddress) 
	    		{
	    		  var filter =  /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	    
			    		if (filter.test(InputEmailAddress,ConfirmEmailAddress))
			    		{
			    			return true;
			    		}
			    		else 
			    		{
			    			return false;
			    		}
	    		}
    		});
});

function processRegistration(){
	var form = document.getElementById("registrationForm");
	var firstName = lastName = emailId = password = "";
	for(var i=0; i<form.length; i++){
		if(form.elements[i].name == "firstname"){
			firstName = form.elements[i].value;	
		}			
		else if(form.elements[i].name == "lastname"){
			lastName = form.elements[i].value;	
		}					
		else if(form.elements[i].name == "InputPassword"){
			password = form.elements[i].value;	
		}			
		else if(form.elements[i].name == "InputEmailAddress"){
			emailId = form.elements[i].value;	
		}			
	}
	if(emailId != null && password != null){
		$.ajax({
			type: "POST",
		    url: "register?firstName="+firstName+"&lastName="+lastName+"&password="+password+"&emailId="+emailId,
		    data: "",
		    dataType: "json",
		    processdata: true,
		    success: function(json){
		    	var jsonObjectError, jsonObjectSucc;
		    	if(json != null){
		    		jsonObjectError = json[0].MAILID_EXISTS_ERROR;	
	    			jsonObjectSucc = json[0].REGISTER_SUCCESS;
		    		/*for(var i=0; i<json.length; i++){
		    			jsonObjectError = json[i].MAILID_EXISTS_ERROR;	
		    			jsonObjectSucc = json[i].REGISTER_SUCCESS;
		    		}*/
		    		if(jsonObjectError != null){
		    			var errorCode = jsonObjectError.ERROR_CODE;
		    			var errorMsg = jsonObjectError.ERROR_MSG;
		    			if(errorCode == "1")
		    				console.log(errorCode + " : "+errorMsg);
		    			document.getElementById("registrationError").innerHTML = errorMsg;		    			
		    		}else if(jsonObjectSucc != null){
		    			var url = window.location.href;
		    			console.log(url);
		    			document.getElementById("registrationError").innerHTML = "";
		    			var url = url.replace("register.jsp",'login.jsp');
		    			$(location).attr('href',url);
		    		}
		    	}
		    }
		});	
	}	
}

   
   