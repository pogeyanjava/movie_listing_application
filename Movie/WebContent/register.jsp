<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Movie List App</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="js/emailvalidation.js"></script>
</head>
<body>
<!-- <header></header>  action="/Movie/register" method="POST" -->
			<ul class="nav nav-pills">
					<li ><a href="Home">Home</a></li>
					<li ><a href="register">Register</a></li>
            		<li><a href="login">Login</a></li>
			
			</ul>
	

<div class="container">
		<div class="page-header"><h1>Registration form</h1></div>
        <div class="row">
			<form id="registrationForm">
						<div class="col-md-8">
							<div class="customclass">
							<label for="firstname">First Name</label>
							<input type="text" id="firstname" class="form-control" name="firstname" placeholder="Enter FirstName"/>
							</div>	
							<div class="customclass-1">
							<label for="lastname">Last Name</label>
							<input type="text" id="lastname" class="form-control" name="lastname" placeholder="Enter LastName"/>
							</div>
						</div>
						<div class="col-md-8">
							<div class="customclass">
							<label for="InputPassword">Enter Password</label>
							<input type="password" id="InputPassword" class="form-control" name="InputPassword" placeholder="Enter Password"/>
							</div>	
							<div class="customclass-1">
							<label for="ConfirmPassword">Confirm Password</label>
							<input type="password"  id="ConfrimPassword" class="form-control" name="ConfirmPassword" placeholder="Confirm Password"/>
							</div>
						</div>
						<div class="col-md-8">
						<div class="customclass">
							<label for="InputEmailAddress">Email Address</label>
							<input type="text" name="InputEmailAddress" id="InputEmailAddress" class="form-control" placeholder="Enter Email address"/>
						</div>	
							<div class="customclass-1">
							<label for="ConfirmEmailAddress">Confirm Email Address</label>
							<input type="text" name="ConfirmEmailAddress" id="ConfirmEmailAddress" class="form-control" placeholder="Confirm Email address"/>
							</div>
						</div>
						<div class="col-md-8">
						 <p id="registrationError" style="color:red"></p>
						</div>
						<div class="col-md-8">
							<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-left" onclick="processRegistration()"/>
					
						</div>
					</form>
					</div>

</div>
<script>
jQuery.validator.setDefaults(
		{
	     debug: true,
	     success: "valid"
	   });
	   $( "#registrationForm" ).validate(
	{
	     rules: 
	     {
	    	firstname:"required",
	    	lastname:"required",
	     	InputPassword: "required",
	     	ConfirmPassword: 
	     	{
	         equalTo: "#InputPassword"
	       },
	       InputEmailAddress: "required",
	       ConfirmEmailAddress:
	       {
	         equalTo:"#InputEmailAddress"
	       }
	     }

	 }
	);
</script>
</body>
</html>